// Essentials(JSX)
import React from "react";
import { StyleSheet, View } from "react-native";
import { Drawer } from "react-native-paper";
// Navigation
import { DrawerContentScrollView, DrawerItem } from "@react-navigation/drawer";

// Configs.
import routes from "./routes";

// Components
import Icon from "../Components/Icon";

function DrawerContent(props) {
  return (
    <View style={styles.DrawerContent}>
      <DrawerContentScrollView {...props}>
        <Drawer.Section>
          <DrawerItem
            label={routes.HOME}
            icon={({ color, size }) => (
              <Icon name="home" color={color} size={size} />
            )}
            onPress={() => props.navigation.navigate(routes.HOME)}
          />
          <DrawerItem
            label={routes.ACCOUNT}
            icon={({ color, size }) => (
              <Icon name="account" color={color} size={size} />
            )}
            onPress={() => props.navigation.navigate(routes.ACCOUNT)}
          />
        </Drawer.Section>
      </DrawerContentScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  DrawerContent: {
    flex: 1,
  },
});

export default DrawerContent;
