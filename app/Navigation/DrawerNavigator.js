// Essentials(JSX)
import React from "react";

// Navigation
import { createDrawerNavigator } from "@react-navigation/drawer";

// Navigator
import TabNavigator from "./TabNavigator";
import DrawerContent from "./DrawerContent";
import routes from "./routes";

const Drawer = createDrawerNavigator();
const DrawerNavigator = () => (
  <Drawer.Navigator drawerContent={(props) => <DrawerContent {...props} />}>
    <Drawer.Screen name={routes.TABS} component={TabNavigator}></Drawer.Screen>
  </Drawer.Navigator>
);

export default DrawerNavigator;
