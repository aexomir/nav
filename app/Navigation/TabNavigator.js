// Essentials (JSX)
import React from "react";
// Navigation
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { useNavigation } from "@react-navigation/native";

// Navigators
import HomeNavigator from "./HomeNavigator";
import AccountNavigator from "./AccountNavigator";
import routes from "./routes";

// Components
import Icon from "../Components/Icon";

// BadgeState
import { getUnSeenMessages } from "../state/messages";
import store from "../state/store";

const unseenMessages = getUnSeenMessages(store.getState());
if (unseenMessages.length > 0) {
  var badgeNumber = unseenMessages.length;
}

const Tab = createBottomTabNavigator();

const TabNavigator = () => {
  const navigation = useNavigation();
  return (
    <Tab.Navigator>
      <Tab.Screen
        name={routes.HOME}
        component={HomeNavigator}
        options={{
          tabBarIcon: ({ size, color }) => (
            <Icon
              name="home"
              size={size}
              color={color}
              size={size}
              onPress={() => navigation.navigate(routes.HOME)}
            />
          ),
          tabBarBadge: badgeNumber,
        }}
      ></Tab.Screen>
      <Tab.Screen
        name={routes.ACCOUNT}
        component={AccountNavigator}
        options={{
          tabBarIcon: ({ size, color }) => (
            <Icon
              name="account"
              size={size}
              color={color}
              size={size}
              onPress={() => navigation.navigate(routes.ACCOUNT)}
            />
          ),
        }}
      ></Tab.Screen>
    </Tab.Navigator>
  );
};

export default TabNavigator;
