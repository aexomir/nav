// Essentials (JSX)
import React from "react";
import { StyleSheet } from "react-native";
// Navigation
import { createStackNavigator } from "@react-navigation/stack";
import { useNavigation } from "@react-navigation/native";

// Screens
import HomeScreen from "../Screens/HomeScreen";
import DetailsScreen from "../Screens/DetailsScreen";
import routes from "./routes";

// Components
import Icon from "../Components/Icon";

// Configs
import colors from "../config/colors";

const Stack = createStackNavigator();

const HomeNavigator = () => {
  const navigation = useNavigation();
  return (
    <Stack.Navigator>
      <Stack.Screen
        name={routes.HOME}
        component={HomeScreen}
        options={{
          headerLeft: () => (
            <Icon
              name="menu"
              size={24}
              color={colors.primary}
              style={styles.icon}
              onPress={() => navigation.toggleDrawer()}
            />
          ),
        }}
      ></Stack.Screen>
      <Stack.Screen
        name={routes.DETAILS}
        component={DetailsScreen}
      ></Stack.Screen>
    </Stack.Navigator>
  );
};

const styles = StyleSheet.create({
  icon: {
    paddingHorizontal: 20,
  },
});

export default HomeNavigator;
