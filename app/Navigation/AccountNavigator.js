// Essentials (JSX)
import React from "react";
// Navigation
import { createStackNavigator } from "@react-navigation/stack";

// Screens
import AccountScreen from "../Screens/AccountScreen";
import routes from "./routes";

const Stack = createStackNavigator();

const AccountNavigator = () => (
  <Stack.Navigator>
    <Stack.Screen
      name={routes.ACCOUNT}
      component={AccountScreen}
    ></Stack.Screen>
  </Stack.Navigator>
);

export default AccountNavigator;
