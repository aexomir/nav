// Essentials
import React, { useContext } from "react";
import { View, StyleSheet } from "react-native";

// Components
import Text from "../Components/Text";
import Button from "../Components/Button";

// Navigation
import routes from "../Navigation/routes";

// State
import StateContext from "../state/stateContext";
import { MessageAdded, MessageSeen, MessageRemoved } from "../state/messages";

export default HomeScreen = ({ navigation }) => {
  const { store } = useContext(StateContext);
  return (
    <View style={styles.container}>
      <Text>Home Screen</Text>
      <Button
        title="go to Details"
        onPress={() => navigation.navigate(routes.DETAILS)}
      />
      <Button
        title="Create New Message"
        onPress={() =>
          store.dispatch(MessageAdded({ message: "Hey! This is a Message" }))
        }
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});
