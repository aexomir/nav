import React from "react";
import { View, StyleSheet } from "react-native";

import Text from "../Components/Text";

export default AccountScreen = (props) => {
  return (
    <View style={styles.container}>
      <Text>Account Screen</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});
