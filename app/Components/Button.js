import React from "react";
import { TouchableOpacity, Text, View, StyleSheet } from "react-native";

// Configs
import colors from "../config/colors";

function AppButton({ style, title, onPress, ...otherProps }) {
  return (
    <View>
      <TouchableOpacity
        onPress={onPress}
        style={[style, styles.button]}
        {...otherProps}
      >
        <Text style={styles.text}>{title}</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  button: {
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 2,
    borderColor: colors.primary,
    padding: 10,
    borderRadius: 20,
    backgroundColor: colors.primary,
  },
  text: {
    color: colors.white,
  },
});

export default AppButton;
