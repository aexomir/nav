import React from "react";
import { TouchableOpacity } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";

function Icon({ style, name, color, size, onPress, ...otherProps }) {
  return (
    <TouchableOpacity onPress={onPress} style={style} {...otherProps}>
      <MaterialCommunityIcons name={name} color={color} size={size} />
    </TouchableOpacity>
  );
}

export default Icon;
