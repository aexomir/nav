import React from "react";
import { View, StyleSheet, Text } from "react-native";

// Config
import colors from "../config/colors";

export default function AppText({ children, style, ...otherProps }) {
  return (
    <View>
      <Text style={[style, styles.text]} {...otherProps}>
        {children}
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {},
  text: {
    fontSize: 16,
    color: colors.primary,
  },
});
