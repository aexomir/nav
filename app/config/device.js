import { useDimensions } from "@react-native-community/hooks";

const { width, height } = useDimensions().window;

export default {
  width,
  height,
};
