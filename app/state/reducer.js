import { combineReducers } from "redux";

import MessagesReducer from "./messages";

export default combineReducers({
  Messages: MessagesReducer,
  // Other Reducers...
});
