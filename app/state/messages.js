import { createAction, createReducer, createSlice } from "@reduxjs/toolkit";

let lastId = 0;

// Action Types
const MESSAGE_ADDED = "messageAdded";
const MESSAGE_SEEN = "seen";
const MESSAGE_REMOVED = "messageRemoved";

// FIRST METHOD:
// Actions
// export const MessageAdded = (message) => ({
//   type: MESSAGE_ADDED,
//   payload: {
//     message,
//   },
// });

// export const MessageRemoved = (id) => ({
//   type: MESSAGE_REMOVED,
//   payload: {
//     id,
//   },
// });

// export const MessageSeen = (id) => ({
//   type: MESSAGE_SEEN,
//   payload: {
//     id,
//   },
// });

//Reducer
// export default MessagesReducer = (state = [], action) => {
//   //
//   if (action.type == MESSAGE_ADDED) {
//     return [
//       ...state,
//       {
//         id: ++lastId,
//         message: action.payload.message,
//         seen: false,
//       },
//     ];
//   }
//   //
//   else if (action.type == MESSAGE_REMOVED)
//     return state.filter((message) => message.id == action.payload.id);
//   //
//   else if (action.type == MESSAGE_SEEN)
//     return produce(state, (draft) => {
//   index = draft.findIndex((message) => message.id == action.payload.id);
//   draft[index].seen = true;
//     });
//   //
//   return state;
// };

// SECOND METHOD:
// Actions
// export const MessageAdded = createAction(MESSAGE_ADDED);
// export const MessageRemoved = createAction(MESSAGE_REMOVED);
// export const MessageSeen = createAction(MESSAGE_SEEN);

// Reducer
// export default MessagesReducer = createReducer([], {
//   [MessageAdded.type]: (state, action) => {
//     state.push({
//       id: ++lastId,
//       message: action.payload.message,
//       seen: false,
//     });
//   },

//   [MessageRemoved.type]: (state, action) => {
//     state.filter((message) => message.id == action.payload.id);
//   },

//   [MessageSeen.type]: (state, action) => {
//     index = state.findIndex((message) => message.id == action.payload.id);
//     state[index].seen = true;
//   },
// });

// THIRD METHOD:
// Slice = Action + Reducer
const Slice = createSlice({
  name: "Messages",
  initialState: [],
  reducers: {
    MessageAdded: (state, action) => {
      state.push({
        id: ++lastId,
        message: action.payload.message,
        seen: false,
      });
    },

    MessageRemoved: (state, action) => {
      state.filter((message) => message.id == action.payload.id);
    },

    MessageSeen: (state, action) => {
      index = state.findIndex((message) => message.id == action.payload.id);
      state[index].seen = true;
    },
  },
});

export default Slice.reducer;
export const { MessageAdded, MessageRemoved, MessageSeen } = Slice.actions;

// TODO: Selector
export const getUnSeenMessages = (state) => {
  return state.Messages.filter((message) => message.seen == false);
};
