// Essentials
import React from "react";
import { StyleSheet, View } from "react-native";
import { NavigationContainer } from "@react-navigation/native";

// Navigation
import DrawerNavigator from "./app/Navigation/DrawerNavigator";

// Configs
import colors from "./app/config/colors";
import Theme from "./app/Navigation/NavigationTheme";

// State
import store from "./app/state/store";
import StateContext from "./app/state/stateContext";

export default function App() {
  console.log(store.getState());
  return (
    <View style={styles.container}>
      <StateContext.Provider value={{ store }}>
        <NavigationContainer theme={Theme}>
          <DrawerNavigator />
        </NavigationContainer>
      </StateContext.Provider>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
});
